export const state = () => ({
  localStream: null,
  isVideoEnabled: false,
  isAudioEnabled: false
})

export const getters = {
  getLocalStream: state => state.localStream,
  isVideoEnabled: state => state.isVideoEnabled,
  isAudioEnabled: state => state.isAudioEnabled
}

export const mutations = {
  SET_LOCAL_STREAM(state, stream) {
    state.localStream = stream
    state.isVideoEnabled = stream.getVideoTracks()[0].enabled
    state.isAudioEnabled = stream.getAudioTracks()[0].enabled
  },

  CHANGE_VIDEO_STATE(state) {
    state.localStream.getVideoTracks()[0].enabled = !state.isVideoEnabled
    state.isVideoEnabled = !state.isVideoEnabled
  },

  CHANGE_AUDIO_STATE(state) {
    state.localStream.getAudioTracks()[0].enabled = !state.isAudioEnabled
    state.isAudioEnabled = !state.isAudioEnabled
  }
}

export const actions = {
  setLocalStream({ commit }, stream) {
    commit('SET_LOCAL_STREAM', stream)
  },

  changeVideoState({ commit }) {
    commit('CHANGE_VIDEO_STATE')
  },

  changeAudioState({ commit }) {
    commit('CHANGE_AUDIO_STATE')
  }
}
