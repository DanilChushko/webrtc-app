module.exports = {
  HOST: 'localhost',
  PORT: '8080',
  API_PORT: 3001,
  WS_URL: 'http://localhost:3001'
}
