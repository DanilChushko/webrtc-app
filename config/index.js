const devConfig = require('./development')

const env = process.env.NODE_ENV || 'development'

const envConfigs = {
  development: devConfig
}

const config = envConfigs[env]

if (!config) {
  throw new TypeError(
    env + ' is not a valid NODE_ENV. Please, use production or develop'
  )
}

module.exports = { ...config }
