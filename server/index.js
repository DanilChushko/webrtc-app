const http = require('http')
const express = require('express')

const app = express()
const server = http.Server(app)
const io = require('socket.io')(server)

const config = require('../config')

io.on('connection', (socket) => {
  socket.on('join-room', ({ roomId, peerId }) => {
    socket.join(roomId)
    socket.to(roomId).broadcast.emit('user-connected', peerId)

    socket.on('disconnect', () => {
      socket.to(roomId).broadcast.emit('user-disconnected', peerId)
    })
  })
})

server.listen(config.API_PORT, () => {
  console.log(`listening on http://localhost:${config.API_PORT}`)
})
